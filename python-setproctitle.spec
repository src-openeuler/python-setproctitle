%global _empty_manifest_terminate_build 0
Name:           python-setproctitle
Version:        1.3.3
Release:        1
Summary:        A Python module to customize the process title
License:        BSD-3-Clause
URL:            https://github.com/dvarrazzo/py-setproctitle
Source0:        https://files.pythonhosted.org/packages/ff/e1/b16b16a1aa12174349d15b73fd4b87e641a8ae3fb1163e80938dbbf6ae98/setproctitle-1.3.3.tar.gz

%description
A Python module to customize the process title

%package -n python3-setproctitle
Summary:        A Python module to customize the process title
Provides:       python-setproctitle = %{version}-%{release}
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-cffi
BuildRequires:  gcc procps
# Tests running requires
BuildRequires:  python3-pytest
Requires:       python3-pytest

%description -n python3-setproctitle
A Python module to customize the process title

%package help
Summary:        A Python module to customize the process title
Provides:       python3-setproctitle-doc
%description help
A Python module to customize the process title

%prep
%autosetup -n setproctitle-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
export PYTHONPATH=%{buildroot}%{python3_sitearch}
%{__python3} -m pytest

%files -n python3-setproctitle -f filelist.lst
%dir %{python3_sitearch}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Mar 18 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 1.3.3-1
- Update package to version 1.3.3

* Fri Apr 28 2023 xu_ping <707078654@qq.com> - 1.3.2-1
- Upgrade package to 1.3.2 version.

* Fri May 13 2022 caodongxia <caodongxia@h-partners.com> - 1.2.2-2
- Add buildrequire procps to fix compile failure 

* Mon Aug 09 2021 OpenStack_SIG <openstack@openeuler.org> - 1.2.2-1
- Upgrade version to 1.2.2

* Wed Aug 04 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.2.1-2
- DESC: delete BuildRequires gdb

* Wed Jan 06 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
